package es.edu.alter.practica0.modelo.spring.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.edu.alter.practica0.modelo.Jugador;
import sia.knights.Knight;

public class JugadorSpringMain {

	public static void main(String[] args) {
	    ClassPathXmlApplicationContext context = 
	            new ClassPathXmlApplicationContext(
	                "META-INF/juego/jugador.xml");
	        Jugador jug1= context.getBean(Jugador.class);
	        System.out.println("jug1=" + jug1);
	        System.out.println("juego=" + jug1.getJugadaElegida().getNombre());
	        context.close();

	}

}
