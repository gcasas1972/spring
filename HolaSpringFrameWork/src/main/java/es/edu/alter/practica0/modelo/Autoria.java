package es.edu.alter.practica0.modelo;

import java.sql.Date;

public class Autoria {
	private Date fecha;
	private int cantidadJugadas;

	public Autoria() {}

	public Autoria(Date fecha, int cantidadJudas) {
		super();
		this.fecha = fecha;
		this.cantidadJugadas = cantidadJudas;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getCantidadJugadas() {
		return cantidadJugadas;
	}

	public void setCantidadJugadas(int cantidadJudas) {
		this.cantidadJugadas = cantidadJudas;
	}
	
	//metodo de negocio
	public void contarDespuesDeLaJugada() {
		cantidadJugadas ++;
	}
	
	

}
