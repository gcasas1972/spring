package com.domain.util.test;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.domain.util.DateUtil;

public class DateUtilTest {
	//lote de pruebas
	Date fecha;

	@Before
	public void setUp() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.set(1972,Calendar.NOVEMBER,17);
		fecha = cal.getTime();		
	}

	@After
	public void tearDown() throws Exception {
		fecha=null;
	}

	@Test
	public void testGetAnio() {
		assertEquals(1972, DateUtil.getAnio(fecha));
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		System.out.println("feha=" + fecha);
		System.out.println("calendar=" + cal);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd-EEEEEE");
		System.out.println("fecha formateada=" + sdf.format(fecha));
		
	}

}
