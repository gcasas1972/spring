package com.baeldung.joinpoint;



import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ArticleService {

    public List<String> getArticleList() {
        return Arrays.asList(
          "Article 1",
          "Article 2"
        );
    }

    public List<String> getArticleList(String startsWithFilter) {
        if (StringUtils.isBlank(startsWithFilter)) {
            throw new IllegalArgumentException("startsWithFilter can't be blank");
        }

        return getArticleList()
          .stream()
          .filter(a -> a.startsWith(startsWithFilter))
          .collect(toList());
    }

}
