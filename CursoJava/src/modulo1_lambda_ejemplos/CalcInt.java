package modulo1_lambda_ejemplos;

@FunctionalInterface
public interface CalcInt {
	Integer op(Integer a, Integer b);
}
