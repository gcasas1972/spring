package modulo1_lambda_ejemplos;

public class Ejemplo1 {

	public static void main(String[] args) {
		System.out.println();
		System.out.print("suma lambda on site 4+2=");
		CalcInt sum = (Integer x, Integer y) -> {
		return x + y;
		};
		CalcInt mult = (Integer x, Integer y) -> x * y;
		CalcInt rest = (x, y) -> x - y;

		System.out.println("sum " + sum.op(6, 4));
		System.out.println("rest " + rest.op(6, 4));
		System.out.println("mult " + mult.op(6, 4));
		}
}