package modulo1_lambda_ejemplos;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Ejemplo2_grupos {

	public static void main(String[] args) {
		List <Integer> li= Arrays.asList(5, 7, 10, 25, 74);		
		int suma=li.stream()
				.mapToInt(x->x.intValue())
				.sum();
		
		System.out.println("Suma de la lista "+suma);
		
		long pares=li
				.stream().filter(x->x % 2 ==0)
				.count();
		System.out.println("Cantidad de pares "+pares);
		
//		Integer[] arrInt=Stream.of(23,45,67,88,2,27)
//					.filter(x->x % 2 ==0)
//					.toArray(Integer[]::new);
		
		Object[] arrInt=Stream.of(23,45,67,88,2,27)
		.filter(x->x % 2 ==0)
		.toArray();

		System.out.println("imprimo todos los valores de la lista");
		li.forEach(x -> System.out.println("valor =" + x))	;
		
		System.out.println("imprime los valores pares");
		li.stream()
		 .filter(x->x%2==0)
		 .forEach(x-> System.out.println("valor par=" + x));

	//	Arrays.stream(arrInt).forEach(x->System.out.println("valor=" + x));        
	}

}
