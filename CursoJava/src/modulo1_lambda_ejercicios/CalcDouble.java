package modulo1_lambda_ejercicios;

@FunctionalInterface
public interface CalcDouble {
	Double operacion(Double a, Double b, Double c);
}
