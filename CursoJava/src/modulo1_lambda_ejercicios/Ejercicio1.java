package modulo1_lambda_ejercicios;

public class Ejercicio1 {
	
	public static void main(String[] args) {
		CalcDouble suma = (x,y,z)->(x+y+z);
		CalcDouble promedio = (x,y,z)->((x+y+z)/3);		
		System.out.println("suma de 7,8,9="+suma.operacion(7.0, 8.0, 9.0));
		System.out.println("prom de 7,8,9="+promedio.operacion(7.0, 8.0, 9.0));
	}

}
