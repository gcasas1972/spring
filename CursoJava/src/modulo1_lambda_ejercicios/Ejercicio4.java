package modulo1_lambda_ejercicios;

import java.util.Arrays;
import java.util.List;

public class Ejercicio4 {

	public static void main(String[] args) {
		List<String> palabras = Arrays.asList("Hola", "como", "te", "va", "soy", "Gabriel", "Casas");
				//                              4        4     2     2      3       7	      5 =27
				//							4
		int totalLetras = palabras	.stream()
									.mapToInt(x->x.length())
									.sum();
		int totalPalamasGrandes =(int) palabras	.stream()
											.filter(x -> x.length()>3)
											.count();
		
		System.out.println("cantida total de letras=" + totalLetras);
		System.out.println("cantidad de palabras con mas de 3 letras=" + totalPalamasGrandes);

	}

}
