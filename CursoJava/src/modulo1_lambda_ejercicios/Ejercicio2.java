package modulo1_lambda_ejercicios;

public class Ejercicio2 {

	public static void main(String[] args) {
		VerifBoolean isTriangulo = (a,b,c)->(a+b>c 	&&
											 a+c>b	&&
											 b+c>a);
		
		VerifBoolean isEquilatero = (a,b,c)->(a.equals(b) &&
											  a.equals(c));
		
		VerifBoolean isRectangulo = (a,b,c)->(	Math.sqrt(a*a + b*b)==c	||
												Math.sqrt(a*a + c*c)==b	||
												Math.sqrt(b*b + c*c)==a	);
		
		System.out.println("isTringulo 3,4,5=" + isTriangulo.operacion(3.0, 4.0, 5.0));
		System.out.println("isTringulo 3,4,1=" + isTriangulo.operacion(3.0, 4.0, 1.0));
		System.out.println("isTringulo 3,4,10=" + isTriangulo.operacion(3.0, 4.0, 10.0));
		
		System.out.println("\nisEquilatero 6,6,6=" + isEquilatero.operacion(6.0, 6.0, 6.0));
		System.out.println("isEquilatero 6,6,8=" + isEquilatero.operacion(6.0, 6.0, 8.0));
		
		System.out.println("\nisRectangulo 6,6,8=" + isRectangulo.operacion(6.0, 6.0, 8.0));
		System.out.println("isRectangulo 3,4,5=" + isRectangulo.operacion(3.0, 4.0, 5.0));
	}

}
