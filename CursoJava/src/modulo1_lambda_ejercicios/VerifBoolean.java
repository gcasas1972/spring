package modulo1_lambda_ejercicios;
@FunctionalInterface
public interface VerifBoolean {
   Boolean operacion(Double a, Double b, Double c);
}
