package modulo1_lambda_ejercicios;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ejercicio3 {

	public static void main(String[] args) {
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(1);
		lista.add(5);
		lista.add(6);
		lista.add(65);
		lista.add(38);
		lista.add(16);
		
		
		int suma = lista.stream()
				.mapToInt(x->x.intValue())
				.sum();
			
		int cantImpares=(int) lista
							.stream()
							.filter(x->x%2==1)
							.count();
		
//		Arrays.stream(impares)
//				.mapToInt(x->((Integer)x).intValue() )
//				.count();
		
		System.out.println("suma =" + suma);
		System.out.println("cantidad de impares=" + cantImpares);
		

	}

}
